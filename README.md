# EMApp by Patrik Mokriš
> Diplomová práce vytvořená dle požadavků domacích úkolů Etnetera Mobile Academy 2021

## Popis aplikace
Aplikace pro platformu Android oprávněným uživatelům dává přehled o účastnících této akademie, vyfiltrování
dle specializace a zobrazení detailu o každém z nich využívajíc data z backendu pomocí HTTP komunikace.

Example: https://www.youtube.com/watch?v=svwoDeHAIXU (Not showing all details)

## Architektura a technologie
Implementace staví na MVVM (Model-View-ViewModel), tedy každá interaktivní část má svoje View
řídící, co se má uživateli zobrazit, které je napojené na ViewModel, který získává/ukládá data z Repository
a manipuluje s nimi. Komunikace mezi View a ViewModelem je zařízena pomocí observování LiveDat a reakce na jejich změnu.
Pro slabé závislosti(DI) je zde využitý Koin framework.

Interaktivní části:
1. ParticipantListFragment (ParticipantListViewModel, ParticipantListRepository)
    - Rychlý přehled všech účastníků s možností filtrování
2. ParticipantFragment (ParticipantViewModel, ParticipantRepository - využívá SQLite databázi, kterou spravuje pomocí android jetpack knihovny Room)
    - Detail účastníka, odkazy na jeho sociální sítě, dovednosti a progress domácéch úkolů
3. LoginActivity (zde pouze LoginViewModel, pouze oveřuje, zda se přihlášení (ne)zdařilo)
    - Aktivita pro autentizaci uživatele aplikace s možností si uložit přihlašovácí údaje do příště

Výčet použitých technologií:
- REST API, OkHttp3, Retrofit2
- Android jetpack Room
- Koin
- Glide
- ResultApi
- Android features: AndroidViewModel, LiveData, Navigation, RecyclerView, SwipeToRefreshLayout, Fragment, Activity
