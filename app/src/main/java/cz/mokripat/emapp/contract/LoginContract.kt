package cz.mokripat.emapp.contract

import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import cz.mokripat.emapp.data.network.model.dto.AccessToken
import cz.mokripat.emapp.view.LoginActivity

class LoginContract : ActivityResultContract<Unit, AccessToken>() {

    override fun parseResult(resultCode: Int, intent: Intent?): AccessToken =
        AccessToken(intent?.getStringExtra(ACCESS_TOKEN) ?: "")


    companion object {
        const val ACCESS_TOKEN = "access_token"
    }

    override fun createIntent(context: Context, input: Unit?): Intent = Intent(context, LoginActivity::class.java)
}