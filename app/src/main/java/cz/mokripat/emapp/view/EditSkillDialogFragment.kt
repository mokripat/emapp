package cz.mokripat.emapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import cz.mokripat.emapp.data.network.model.Skills
import cz.mokripat.emapp.databinding.EditSkillsDialogLayoutBinding

class EditSkillDialogFragment : DialogFragment() {

    companion object {
        const val RESULT_SKILLS = "result_skills"
        const val KEY_SKILLS = "key_skills"
    }

    private lateinit var binding: EditSkillsDialogLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = EditSkillsDialogLayoutBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.confirmButton.setOnClickListener {
            sendBackResult()
        }
    }

    // Call this method to send the data back to the parent fragment
    private fun sendBackResult() {

        val skills = Skills(
            binding.editSwiftSlider.value.toInt(),
            binding.editIOSSlider.value.toInt(),
            binding.editAndroidSlider.value.toInt(),
            binding.editKotlinSlider.value.toInt()
        )

        parentFragmentManager.setFragmentResult(RESULT_SKILLS, bundleOf(KEY_SKILLS to skills))
        dismiss()
    }
}