package cz.mokripat.emapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton
import cz.mokripat.emapp.R
import cz.mokripat.emapp.contract.LoginContract
import cz.mokripat.emapp.data.OnRowListener
import cz.mokripat.emapp.data.ParticipantsAdapter
import cz.mokripat.emapp.data.network.RestClient.Companion.accessToken
import cz.mokripat.emapp.data.network.model.dto.AccessToken
import cz.mokripat.emapp.databinding.FragmentParticipantlistBinding
import cz.mokripat.emapp.utils.Resource
import cz.mokripat.emapp.viewmodel.ParticipantListViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ParticipantListFragment : Fragment(), OnRowListener {
    private lateinit var adapter: ParticipantsAdapter
    private lateinit var loginUser: ActivityResultLauncher<Unit?>

    private val viewModel: ParticipantListViewModel by viewModel()

    private var _binding: FragmentParticipantlistBinding? = null

    private val binding get() = checkNotNull(_binding)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentParticipantlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("debug", "onViewCreated called ")
        super.onViewCreated(view, savedInstanceState)

        adapter = ParticipantsAdapter(emptyList(), this)
        binding.recyclerView.root.adapter = adapter


        //registration of login activity RESULT
        loginUser = registerForActivityResult(LoginContract()) {
            if (it != null) {
                val text = getString(R.string.login_success_msg)
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(context, text, duration)
                toast.show()
                accessToken = AccessToken(it.access_token)
                Log.d("loginResult", "${it.access_token} ")
                viewModel.refreshParticipantListData()
            }
        }

        //filter buttons are accessed from parent
        val allButton = binding.allButton
        val androidButton = binding.androidButton
        val iosButton = binding.iosButton

        val refreshLayout = binding.refreshLayout

        refreshLayout.setOnRefreshListener {
            viewModel.refreshParticipantListData()
            refreshLayout.isRefreshing = false
        }

        allButton.setOnClickListener {
            viewModel.filterAllParticipants()
            unhighlightButton(androidButton)
            unhighlightButton(iosButton)
        }

        androidButton.setOnClickListener {
            viewModel.filterAndroidParticipants()
            highlightAndroid()
        }

        iosButton.setOnClickListener {
            viewModel.filterIOSParticipants()
            highlightIOS()
        }

        viewModel.participantListLive.observe(viewLifecycleOwner) {
            adapter.participants = it
        }

        viewModel.refreshResourceLive.observe(viewLifecycleOwner) {
            refreshParticipantListView(it)
        }

        viewModel.refreshParticipantListData()
    }

    //downloads the participants list data with handling possible errors
    private fun refreshParticipantListView(res: Resource) {
        binding.progressCircularList.isVisible = false
        binding.wifiErrorLayout.root.isVisible = false

        Log.d("observe", "$res ")
        when (res) {
            Resource.Loading -> {
                binding.recyclerView.root.visibility = View.GONE
                binding.progressCircularList.visibility = View.VISIBLE
            }
            Resource.Success -> {
                Log.d("observe", "success ")
                binding.progressCircularList.visibility = View.GONE
                binding.recyclerView.root.visibility = View.VISIBLE
            }
            Resource.IOError -> {
                viewErrorHandle()
            }
            Resource.NetworkError -> {
                binding.wifiErrorLayout.root.isVisible = true
                viewErrorHandle()
            }
            Resource.LoginNeeded -> loginUser.launch(Unit)
            else -> {
            }
        }
        Log.d("refresh", "resource on end of refres: $res")
    }

    private fun viewErrorHandle() {
        binding.progressCircularList.isVisible = false
        binding.recyclerView.root.visibility = View.GONE
    }


    override fun onRowClicked(id: String) {
        Log.d("debug", "onRowClicked: $id")
        val bundle = bundleOf(
            ParticipantFragment.KEY_ID to id
        )

        findNavController().navigate(R.id.action_show_participant_detail, bundle)
    }

    private fun highlightAndroid() {
        val color = ContextCompat.getColor(requireContext(), R.color.android_green)
        binding.androidButton.setBackgroundColor(color)
        unhighlightButton(binding.iosButton)
    }

    private fun highlightIOS() {
        val color = ContextCompat.getColor(requireContext(), R.color.darker_grey)
        binding.iosButton.setBackgroundColor(color)
        unhighlightButton(binding.androidButton)
    }

    private fun unhighlightButton(button: MaterialButton) {
        val color = ContextCompat.getColor(requireContext(), R.color.red)
        button.setBackgroundColor(color)

    }
}