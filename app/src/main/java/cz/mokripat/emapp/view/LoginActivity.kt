package cz.mokripat.emapp.view

import android.app.Activity
import android.os.Bundle
import android.os.StrictMode
import android.text.method.PasswordTransformationMethod
import android.view.animation.CycleInterpolator
import android.view.animation.TranslateAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import cz.mokripat.emapp.R
import cz.mokripat.emapp.databinding.LoginActivityLayoutBinding
import cz.mokripat.emapp.utils.Resource
import cz.mokripat.emapp.viewmodel.LoginViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: LoginActivityLayoutBinding

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginActivityLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.passwordInputField.transformationMethod = PasswordTransformationMethod()

        binding.loginButton.setOnClickListener {
            viewModel.performLogin(
                binding.userIdInputField.text.toString(),
                binding.passwordInputField.text.toString()
            )
        }

        //easterEgg
        binding.etneteraLogo.setOnClickListener {
            logoRotation()
        }

        viewModel.shouldFillUp.observe(this) {
            binding.userIdInputField.setText(it.userId)
            binding.passwordInputField.setText(it.password)
            binding.rememberCheckbox.isChecked = true
        }

        viewModel.refreshResourceLive.observe(this) {
            if (it == Resource.Success) {
                if (binding.rememberCheckbox.isChecked) {
                    viewModel.saveCredentials(
                        binding.userIdInputField.text.toString(),
                        binding.passwordInputField.text.toString()
                    )
                } else {
                    viewModel.clearCredentials()
                }
                setResult(Activity.RESULT_OK, viewModel.resultIntent)
                finish()
            } else {
                val text = getString(R.string.login_failed_msg)
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(applicationContext, text, duration)
                toast.show()

                binding.userIdInputField.startAnimation(shakeAnim())
                binding.passwordInputField.startAnimation(shakeAnim())
                binding.etneteraLogo.startAnimation(shakeAnim())
            }
        }

        strictModeOn()
    }

    private fun shakeAnim(): TranslateAnimation {
        val shake = TranslateAnimation(0f, 10f, 0f, 0f)
        shake.duration = 500
        shake.interpolator = CycleInterpolator(7f)
        return shake
    }

    private fun logoRotation() {
        binding.etneteraLogo.animate().apply {
            duration = 1000
            rotationYBy(360f)
            rotationXBy(360f)
        }.start()
    }

    private fun strictModeOn() {
        StrictMode.setThreadPolicy(
            StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectAll()
                .penaltyLog()
                .build()
        )
        StrictMode.setVmPolicy(
            StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .build()
        )
    }

    companion object {
        const val SHARED_PREFERENCES_NAME = "shared_prefs"
        const val SP_ACCESS_TOKEN = "access_token"
        const val SP_REMEMBER = "remember"
        const val SP_USER_ID = "user_id"
        const val SP_PASSWORD = "password"
    }
}