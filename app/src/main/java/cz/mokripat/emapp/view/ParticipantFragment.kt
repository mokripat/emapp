package cz.mokripat.emapp.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import cz.mokripat.emapp.R
import cz.mokripat.emapp.animation.ProgressBarAnimation
import cz.mokripat.emapp.contract.LoginContract
import cz.mokripat.emapp.data.network.RestClient
import cz.mokripat.emapp.data.network.model.HomeworkType
import cz.mokripat.emapp.data.network.model.ParticipantDetail
import cz.mokripat.emapp.data.network.model.Skills
import cz.mokripat.emapp.data.network.model.dto.AccessToken
import cz.mokripat.emapp.databinding.ParticipantDetailBinding
import cz.mokripat.emapp.utils.Resource
import cz.mokripat.emapp.viewmodel.ParticipantViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.viewmodel.ext.android.viewModel

class ParticipantFragment : Fragment() {
    private var _binding: ParticipantDetailBinding? = null

    private val binding get() = checkNotNull(_binding)

    private lateinit var loginUser: ActivityResultLauncher<Unit>

    private val viewModel: ParticipantViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ParticipantDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = arguments?.getString(KEY_ID)

        loginUser = registerForActivityResult(LoginContract()) {
            if (it != null) {
                RestClient.accessToken = AccessToken(it.access_token)
                viewModel.refreshParticipantData(id)
            }
        }

        binding.detailRefreshLayout.setOnRefreshListener {
            viewModel.refreshParticipantData(id)
            binding.detailRefreshLayout.isRefreshing = false
        }

        binding.participantDetail.editSkillsButton.setOnClickListener {
            EditSkillDialogFragment().show(childFragmentManager, "fragment_edit_skills")
        }

        childFragmentManager.setFragmentResultListener(
            EditSkillDialogFragment.RESULT_SKILLS,
            this
        ) { _, bundle ->
            val skills = bundle.getParcelable<Skills>(EditSkillDialogFragment.KEY_SKILLS)
            if (skills != null) {
                viewModel.updateSkills(id, skills)
                showEditedSkills(skills)
            }
        }

        binding.participantDetail.toggleFavoriteButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.participantDetail.animationView.playAnimation()
                viewModel.saveToFavorites()
                toggleButtonAnim()
            } else {
                viewModel.deleteFromFavorites()
                toggleButtonAnim()
            }
        }

        viewModel.refreshResourceLive.observe(viewLifecycleOwner) {
            refreshView(it, id)
        }

        viewModel.refreshParticipantData(id)
    }

    private fun refreshView(res: Resource, id: String?) {
        binding.loadingErrorView.root.isVisible = false
        binding.wifiErrorLayout.root.isVisible = false
        binding.participantDetail.root.isVisible = true
        binding.participantDetail.toggleFavoriteButton.isVisible = false

        when (res) {
            Resource.Loading -> showLoading()
            Resource.Success -> showData()
            Resource.IOError -> showError()
            Resource.NetworkError -> showWifiError()
            Resource.LoginNeeded -> loginUser.launch(Unit)
            Resource.Unauthorized -> handle403(id)
        }
    }

    private fun handle403(id: String?) {
        val text = getString(R.string.unauth_edit_error)
        val duration = Toast.LENGTH_SHORT
        val toast = Toast.makeText(requireContext(), text, duration)
        toast.show()
        viewModel.refreshParticipantData(id)
    }

    private fun showLoading() {
        binding.participantDetail.profileImage.isVisible = false
        binding.participantDetail.progressCircular.isVisible = true
    }

    private fun showError() {
        binding.loadingErrorView.root.isVisible = true
        binding.participantDetail.root.isVisible = false
    }

    private fun showWifiError() {
        binding.wifiErrorLayout.root.isVisible = true
        binding.participantDetail.root.isVisible = false
    }

    private fun setToggleButtonToColor(resource: Int) {
        context?.let {
            val color = ContextCompat.getColor(
                it,
                resource
            )
            binding.participantDetail.toggleFavoriteButton.setBackgroundColor(color)
        }
    }

    private fun showData() {
        val participant = viewModel.participant
        Glide.with(binding.participantDetail.profileImage).load(participant.icon512)
            .error(R.drawable.error_icon_old)
            .into(binding.participantDetail.profileImage)
        binding.participantDetail.progressCircular.isVisible = false
        binding.participantDetail.profileImage.isVisible = true
        binding.participantDetail.toggleFavoriteButton.isVisible = true
        binding.participantDetail.cardFullname.text = participant.name
        binding.participantDetail.cardStatus.text = participant.title

        lifecycleScope.launch(Dispatchers.IO) {
            if (viewModel.isFavorite())
                withContext(Dispatchers.Main) {
                    binding.participantDetail.toggleFavoriteButton.isChecked = true
                }
        }

        //setting up detail
        Log.d("detail", "getPerson: skills - ${participant.skills}")
        setupSkills(participant)

        //setting up socials
        setupEmail(participant)
        setupSlack(participant)
        setupLinkedIn(participant)

        setupHomeworks(participant)

        Log.d("detail", "getPerson: homeworks - ${participant.homework}")
        Log.d("detail", "getPerson: SUCCESS - $participant")
    }

    private fun setupLinkedIn(participant: ParticipantDetail) {
        binding.participantDetail.linkedInButton.isVisible = participant.linkednUrl != null

        binding.participantDetail.linkedInButton.setOnClickListener {
            val url = Uri.parse(participant.linkednUrl)
            val intent = Intent(Intent.ACTION_VIEW, url)
            if (intent.resolveActivity(requireContext().packageManager) != null) {
                startActivity(intent)
            }
        }
    }

    private fun setupEmail(participant: ParticipantDetail) {
        Log.d("setup", "email: ${participant.email}")
        binding.participantDetail.emailButton.isVisible = participant.email != null

        binding.participantDetail.emailButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(participant.email))
            if (intent.resolveActivity(requireContext().packageManager) != null) {
                startActivity(intent)
            }
        }
    }

    private fun setupSlack(participant: ParticipantDetail) {
        binding.participantDetail.slackButton.isVisible = participant.slackURL != null

        binding.participantDetail.slackButton.setOnClickListener {
            val url = Uri.parse(participant.slackURL)
            val intent = Intent(Intent.ACTION_VIEW, url)
            if (intent.resolveActivity(requireContext().packageManager) != null) {
                startActivity(intent)
            }
        }
    }

    private fun setupSkills(participant: ParticipantDetail) {
        view?.let { _ ->
            showEditedSkills(participant.skills)
        }
    }

    //TODO READY AND COMING SOON homeworks statuses
    private fun setupHomeworks(participant: ParticipantDetail) {
        view?.let { view ->
            val homeworksLayout = view.findViewById<LinearLayout>(R.id.homeworks)
            repeat(participant.homework.size) { index ->
                val homework = homeworksLayout.getChildAt(index)
                when (participant.homework[index].state) {
                    HomeworkType.ACCEPTANCE -> {
                        homework.findViewById<ImageView>(R.id.pushcheck)
                            .setImageResource(R.drawable.baseline_check_circle_black_18dp)
                        homework.findViewById<ImageView>(R.id.reviewcheck)
                            .setImageResource(R.drawable.baseline_check_circle_black_18dp)
                        homework.findViewById<ImageView>(R.id.acceptancecheck)
                            .setImageResource(R.drawable.baseline_check_circle_black_18dp)
                    }
                    HomeworkType.REVIEW -> {
                        homework.findViewById<ImageView>(R.id.pushcheck)
                            .setImageResource(R.drawable.baseline_check_circle_black_18dp)
                        homework.findViewById<ImageView>(R.id.reviewcheck)
                            .setImageResource(R.drawable.baseline_check_circle_black_18dp)
                    }
                    HomeworkType.PUSH -> homework.findViewById<ImageView>(R.id.pushcheck)
                        .setImageResource(R.drawable.baseline_check_circle_black_18dp)
                    else -> {
                    }
                }
            }
        }
    }

    private fun toggleButtonAnim() {
        lifecycleScope.launch {
            binding.participantDetail.toggleFavoriteButton.animate().apply {
                duration = 500
                rotationXBy(540f)
            }.start()
        }
    }

    companion object {
        const val ANIMATION_TIME = 1000L
        const val KEY_ID = "participant_id"
    }

    private fun showEditedSkills(skills: Skills?) {
        val skillz = skills ?: Skills(0, 0, 0, 0)

        //since binding is lateinit I dont know how make this static or to have just one instance
        val skillsViewBinds = listOf(
            binding.participantDetail.skillsPanel.kotlinProgressBar to binding.participantDetail.skillsPanel.kotlinProgressText,
            binding.participantDetail.skillsPanel.androidProgressBar to binding.participantDetail.skillsPanel.androidProgressText,
            binding.participantDetail.skillsPanel.swiftProgressBar to binding.participantDetail.skillsPanel.swiftProgressText,
            binding.participantDetail.skillsPanel.appleProgressBar to binding.participantDetail.skillsPanel.appleProgressText,
        )
        val skillsList = listOf(skillz.kotlin, skillz.android, skillz.swift, skillz.ios)

        repeat(4) { index ->
            val (progressBarBind, textViewBind) = skillsViewBinds[index]
            val skill = skillsList[index]
            val progressBar: ProgressBar = progressBarBind
            val anim = ProgressBarAnimation(
                progressBar,
                progressBar.progress.toFloat(),
                (skill * 10).toFloat()
            )
            anim.duration = ANIMATION_TIME
            progressBar.startAnimation(anim)
            textViewBind.text =
                getString(R.string.progress_template, skill)
        }

    }
}