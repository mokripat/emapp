package cz.mokripat.emapp.repository

import android.util.Log
import cz.mokripat.emapp.data.database.FavParticipant
import cz.mokripat.emapp.data.database.FavParticipantDao
import cz.mokripat.emapp.data.network.api.ParticipantsApi
import cz.mokripat.emapp.data.network.model.ParticipantDetail
import cz.mokripat.emapp.data.network.model.Skills

class ParticipantRepository(
    var api: ParticipantsApi,
    private val userDao: FavParticipantDao
) {

    lateinit var data: ParticipantDetail
        private set

    suspend fun fetchParticipant(id: String): ParticipantDetail {
        data = api.getParticipant(id, false, false)
        return data
    }

    suspend fun postSkills(id: String, skills: Skills) {
        api.postSkills(id,skills)
    }

    fun isFavorite(id: String, name: String): Boolean {
        return userDao.findById(id)?.name == name
    }

    fun saveToFavorites(id: String, name: String) {
        Log.d("database", "saving: $id $name ")
        userDao.insert(FavParticipant(id, name))
    }

    fun deleteFromFavorites(id: String) {
        Log.d("database", "deleting: $id")
        userDao.deleteById(id)
    }
}