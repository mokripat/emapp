package cz.mokripat.emapp.repository

import cz.mokripat.emapp.data.network.api.ParticipantsApi
import cz.mokripat.emapp.data.network.model.Participant
import cz.mokripat.emapp.data.network.model.ParticipantType

class ParticipantListRepository(
    private val api: ParticipantsApi
){
    var data: List<Participant> = listOf()
        private set

    suspend fun fetchParticipants() : List<Participant> {
        data = api.getParticipants(false,true)
        return data
    }

    fun getAndroidParticipants() : List<Participant> {
        return data.filter { it.participantType == ParticipantType.ANDROID_STUDENT || it.participantType == ParticipantType.ANDROID_MENTOR }
    }

    fun getIOsParticipants() : List<Participant> {
        return data.filter { it.participantType == ParticipantType.IOS_STUDENT || it.participantType == ParticipantType.IOS_MENTOR }
    }
}