package cz.mokripat.emapp.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import cz.mokripat.emapp.data.network.RestClient
import cz.mokripat.emapp.data.network.model.Participant
import cz.mokripat.emapp.repository.ParticipantListRepository
import cz.mokripat.emapp.utils.NetworkUtils
import cz.mokripat.emapp.utils.ParticipantListState
import cz.mokripat.emapp.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

//THIS IS SO LOOOOONG NAME
class ParticipantListViewModel(
    app: Application,
    private val participantListRepository: ParticipantListRepository
) : AndroidViewModel(app) {

    private val _participantListLive: MutableLiveData<List<Participant>> = MutableLiveData()

    val participantListLive: LiveData<List<Participant>>
        get() = _participantListLive

    private var state: ParticipantListState = ParticipantListState.ALL

    private val _refreshResourceLive: MutableLiveData<Resource> = MutableLiveData()

    val refreshResourceLive: LiveData<Resource>
        get() = _refreshResourceLive


    private var refreshJob: Job? = null

    fun filterAllParticipants() {
        state = ParticipantListState.ALL
        _participantListLive.postValue(participantListRepository.data)
    }

    fun filterAndroidParticipants() {
        state = ParticipantListState.ANDROID
        _participantListLive.postValue(participantListRepository.getAndroidParticipants())
    }

    fun filterIOSParticipants() {
        state = ParticipantListState.IOS
        _participantListLive.postValue(participantListRepository.getIOsParticipants())
    }

    fun refreshParticipantListData() {
        refreshJob?.cancel()
        refreshJob = viewModelScope.launch(Dispatchers.IO) {
            Log.d("resource", "Resource.Loading")
            _refreshResourceLive.postValue(Resource.Loading)
            try {
                _participantListLive.postValue(participantListRepository.fetchParticipants())
                when (state) {
                    ParticipantListState.ANDROID -> filterAndroidParticipants()
                    ParticipantListState.IOS -> filterIOSParticipants()
                    else -> filterAllParticipants()
                }
                _refreshResourceLive.postValue(Resource.Success)
            } catch (exception: IOException) {
                _refreshResourceLive.postValue( if (!NetworkUtils(getApplication()).isConnected()) {
                    Log.d("resource", "Resource.NetworkError")
                    Resource.NetworkError
                } else {
                    Log.d("resource", "Resource.IOError")
                    Resource.IOError
                })
                Log.d("debug", "fetchParticipants: FAILURE - IO EXCEPTION")
            } catch (exception: HttpException) {
                //if 401 -> login activity
                if (exception.code() == 401) {
                    Log.d("resource", "Resource.Needlogin")
                    _refreshResourceLive.postValue(Resource.LoginNeeded)
                    Log.d("debug", "fetchParticipants: 401")
                } else {
                    Log.d("resource", "Resource.IOError")
                    _refreshResourceLive.postValue(Resource.IOError)
                    Log.d("debug", "fetchParticipants: FAILURE - HTTP EXCEPTION")
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        refreshJob?.cancel()
    }
}