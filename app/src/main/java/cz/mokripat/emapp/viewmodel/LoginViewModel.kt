package cz.mokripat.emapp.viewmodel

import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import androidx.lifecycle.*
import cz.mokripat.emapp.contract.LoginContract
import cz.mokripat.emapp.data.network.api.AuthApi
import cz.mokripat.emapp.data.network.model.dto.AccessToken
import cz.mokripat.emapp.data.network.model.dto.LoginRequest
import cz.mokripat.emapp.utils.CredentialsLoader
import cz.mokripat.emapp.utils.Resource
import cz.mokripat.emapp.view.LoginActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

class LoginViewModel(
    private val sharedPrefs: SharedPreferences,
    private val loginApi: AuthApi,
    private val credentialsLoader: CredentialsLoader
) : ViewModel() {

    private val _shouldFillUp: MutableLiveData<CredentialsLoader> = MutableLiveData()

    val shouldFillUp: LiveData<CredentialsLoader>
        get() = _shouldFillUp

    private val _refreshResourceLive: MutableLiveData<Resource> = MutableLiveData()

    val refreshResourceLive: LiveData<Resource>
        get() = _refreshResourceLive

    init {
        _shouldFillUp.postValue(credentialsLoader)
        credentialsLoader.load(viewModelScope)
    }

    val resultIntent = Intent()

    fun performLogin(userIdInput: String, passwordInput: String) {
        viewModelScope.launch {
            try {
                val result = loginApi.login(LoginRequest(userIdInput, passwordInput))
                //passing token to the result of login activity
                resultIntent.apply {
                    putExtra(LoginContract.ACCESS_TOKEN, result.access_token)
                }
                Log.d("login", "performLogin: ${result.access_token}")
                saveToken(result)
                _refreshResourceLive.value = Resource.Success
                return@launch
            } catch (ex: IOException) {
                Log.d("login", "performLogin: IO EXCEPTION")
            } catch (http: HttpException) {
                Log.d("login", "performLogin: HTTP EXCEPTION WITH CODE ${http.code()}")
            }
            _refreshResourceLive.value = Resource.IOError

        }
    }

    private fun saveToken(result: AccessToken) {
        viewModelScope.launch(Dispatchers.IO) {
            sharedPrefs.edit {
                putString(LoginActivity.SP_ACCESS_TOKEN, result.access_token)
            }
        }
    }

    fun saveCredentials(userId: String, password: String) {
        credentialsLoader.save(viewModelScope, userId, password)
    }

    fun clearCredentials() {
        credentialsLoader.clear(viewModelScope)
    }
}