package cz.mokripat.emapp.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import cz.mokripat.emapp.data.network.model.ParticipantDetail
import cz.mokripat.emapp.data.network.model.Skills
import cz.mokripat.emapp.repository.ParticipantRepository
import cz.mokripat.emapp.utils.NetworkUtils
import cz.mokripat.emapp.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

class ParticipantViewModel(
    app: Application,
    private val participantRepository: ParticipantRepository
) : AndroidViewModel(app) {

    lateinit var participant: ParticipantDetail
        private set

    private val _refreshResourceLive: MutableLiveData<Resource> = MutableLiveData()

    val refreshResourceLive: LiveData<Resource>
        get() = _refreshResourceLive

    private var refreshJob: Job? = null

    fun isFavorite() =
        participantRepository.isFavorite(
            participant.id,
            participant.name
        )

    fun saveToFavorites() {
        viewModelScope.launch(Dispatchers.IO) {
            participantRepository.saveToFavorites(
                participant.id,
                participant.name
            )
        }
    }

    fun deleteFromFavorites() {
        viewModelScope.launch(Dispatchers.IO) {
            participantRepository.deleteFromFavorites(participant.id)
        }
    }

    fun refreshParticipantData(id: String?) {
        refreshJob?.cancel()
        refreshJob = viewModelScope.launch(Dispatchers.IO) {
            _refreshResourceLive.postValue(Resource.Loading)
            try {
                if (id == null) throw IOException("id is null")
                participant = participantRepository.fetchParticipant(id)
                _refreshResourceLive.postValue(Resource.Success)
                Log.d("debug", "fetchParticipant: $participant")
            } catch (exception: IOException) {
                if (!NetworkUtils(getApplication()).isConnected()) {
                    _refreshResourceLive.postValue(Resource.NetworkError)
                } else {
                    _refreshResourceLive.postValue(Resource.IOError)
                }
                Log.d("detail", "getPerson: FAILURE")
            } catch (exception: HttpException) {
                if (exception.code() == 401) {
                    _refreshResourceLive.postValue(Resource.LoginNeeded)
                    //loginUser.launch(AccessToken(""))
                    Log.d("debug", "fetchParticipant: 401")
                } else {
                    _refreshResourceLive.postValue(Resource.IOError)
                    Log.d("debug", "fetchParticipant: FAILURE")
                }
            }
        }
    }

    fun updateSkills(id: String?, skills: Skills) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                if (id == null) throw IOException("id is null")
                participantRepository.postSkills(id,skills)
                Log.d("debug", "fetchParticipant: $participant")
            } catch (exception: IOException) {
                if (!NetworkUtils(getApplication()).isConnected()) {
                    _refreshResourceLive.postValue(Resource.NetworkError)
                } else {
                    _refreshResourceLive.postValue(Resource.IOError)
                }
                Log.d("detail", "getPerson: FAILURE")
            } catch (exception: HttpException) {
                if (exception.code() == 401) {
                    _refreshResourceLive.postValue(Resource.LoginNeeded)
                    Log.d("debug", "fetchParticipant: 401")
                } else if(exception.code() == 403) {
                    _refreshResourceLive.postValue(Resource.Unauthorized)
                } else {
                    _refreshResourceLive.postValue(Resource.IOError)
                    Log.d("debug", "fetchParticipant: FAILURE")
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        refreshJob?.cancel()
    }
}