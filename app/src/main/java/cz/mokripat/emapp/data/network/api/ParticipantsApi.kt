package cz.mokripat.emapp.data.network.api

import cz.mokripat.emapp.data.network.model.Participant
import cz.mokripat.emapp.data.network.model.ParticipantDetail
import cz.mokripat.emapp.data.network.model.Skills
import retrofit2.http.*

interface ParticipantsApi {

    @GET("v2/participants")
    suspend fun getParticipants(@Query("badServer") badServer: Boolean,
                                @Query("sleepy") sleepy: Boolean
    ): List<Participant>

    @GET("v2/participants/{id}")
    suspend fun getParticipant(
            @Path("id") id: String,
            @Query("badServer") badServer: Boolean,
            @Query("sleepy") sleepy: Boolean
    ): ParticipantDetail

    @POST("v2/participants/{id}/skills")
    suspend fun postSkills(
        @Path("id") id: String,
        @Body skills: Skills
    )
}