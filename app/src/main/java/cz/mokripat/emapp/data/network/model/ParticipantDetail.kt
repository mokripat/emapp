package cz.mokripat.emapp.data.network.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParticipantDetail(
        val id: String,
        val name: String,
        val participantType: ParticipantType,
        val title: String,
        val icon192: String,
        val icon512: String,
        val homework: List<Homework>,
        val skills: Skills?,
        val email: String?,
        val slackURL : String?,
        val linkednUrl: String?

) : Parcelable
