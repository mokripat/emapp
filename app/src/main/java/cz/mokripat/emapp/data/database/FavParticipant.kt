package cz.mokripat.emapp.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavParticipant(
    @PrimaryKey
    var user_id: String,
    var name: String
)