package cz.mokripat.emapp.data.network.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Participant(
        val id: String,
        val name: String,
        val participantType: ParticipantType,
        val title: String,
        val icon192: String,
) : Parcelable

