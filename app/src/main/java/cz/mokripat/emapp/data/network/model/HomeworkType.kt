package cz.mokripat.emapp.data.network.model

import com.google.gson.annotations.SerializedName

enum class HomeworkType {
    @SerializedName("push")
    PUSH,

    @SerializedName("ready")
    READY,

    @SerializedName("acceptance")
    ACCEPTANCE,

    @SerializedName("comingsoon")
    COMINGSOON,

    @SerializedName("review")
    REVIEW,
}