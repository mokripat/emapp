package cz.mokripat.emapp.data.network

import android.app.Application
import android.content.Context
import android.util.Log
import cz.mokripat.emapp.data.network.api.AuthApi
import cz.mokripat.emapp.data.network.api.ParticipantsApi
import cz.mokripat.emapp.data.network.model.dto.AccessToken
import cz.mokripat.emapp.view.LoginActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestClient(app: Application) {

    init {
        initFromSharedPrefs(app)
    }

    private fun createRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(EMA_API_BASEURL)
            .client(createClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder()
            .addInterceptor {
                val request = it.request().newBuilder().addHeader(
                    ACCESS_TOKEN_HEADER,
                    accessToken.access_token
                ).build()
                it.proceed(request)
            }
            .addInterceptor(interceptor)
            .addInterceptor(interceptor500())
            .build()
    }

    private fun interceptor500(): Interceptor {
        return Interceptor() {
            val request = it.request()
            val response = it.proceed(request)
            if (response.code == 500) {
                Log.d("debug", "500 caught by inteceptor")
            }
            response
        }
    }

    private fun initFromSharedPrefs(context: Context)
    {
        runBlocking {
            val preferences = context
                .getSharedPreferences(LoginActivity.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
            val savedAccessToken = withContext(Dispatchers.IO) {
                preferences.getString(LoginActivity.SP_ACCESS_TOKEN, "failed")
            }
            //if access token is stored, it tries using it
            savedAccessToken?.let {
                accessToken = AccessToken(savedAccessToken)
            }
        }
    }

    val participantsApi: ParticipantsApi by lazy {
        createRetrofit().create(ParticipantsApi::class.java)
    }

    val authApi: AuthApi by lazy {
        createRetrofit().create(AuthApi::class.java)
    }

    companion object {
        const val EMA_API_BASEURL = "http://emarest.cz.mass-php-1.mit.etn.cz/api/"
        const val ACCESS_TOKEN_HEADER = "access_token"
        var accessToken : AccessToken = AccessToken("")

    }

}