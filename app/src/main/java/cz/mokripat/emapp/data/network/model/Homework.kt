package cz.mokripat.emapp.data.network.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Homework(
        val state: HomeworkType,
        val number: Int
) : Parcelable
