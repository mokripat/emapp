package cz.mokripat.emapp.data.network.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Skills (
    val swift: Int,
    val ios: Int,
    val android: Int,
    val kotlin: Int
        ) : Parcelable