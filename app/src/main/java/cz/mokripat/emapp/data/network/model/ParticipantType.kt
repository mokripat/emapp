package cz.mokripat.emapp.data.network.model

import com.google.gson.annotations.SerializedName

enum class ParticipantType {
    @SerializedName("iosMentor")
    IOS_MENTOR,

    @SerializedName("iosStudent")
    IOS_STUDENT,

    @SerializedName("androidMentor")
    ANDROID_MENTOR,

    @SerializedName("androidStudent")
    ANDROID_STUDENT,

    @SerializedName("watcher")
    WATCHER,
}