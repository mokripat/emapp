package cz.mokripat.emapp.data.database

import androidx.room.*

@Dao
interface FavParticipantDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favParticipant: FavParticipant)

    @Query("DELETE FROM favparticipant WHERE user_id=:id")
    fun deleteById(id: String)

    @Query("SELECT * FROM FavParticipant where user_id=:id")
    fun findById(id: String) : FavParticipant?

    @Query("SELECT * FROM FavParticipant")
    fun getAll(): List<FavParticipant>
}