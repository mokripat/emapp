package cz.mokripat.emapp.data.network.model.dto

data class AccessToken(val access_token: String)