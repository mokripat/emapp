package cz.mokripat.emapp.data.network.model.dto

data class LoginRequest(
    val user_id: String,
    val password: String
)