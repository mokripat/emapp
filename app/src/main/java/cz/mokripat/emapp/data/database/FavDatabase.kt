package cz.mokripat.emapp.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(version = 1, entities = [FavParticipant::class])
abstract class FavDatabase : RoomDatabase() {
    abstract fun getFavParticipantDao(): FavParticipantDao


    companion object {

        private const val DB_NAME = "fav_database.db"

        fun create(context: Context) =
            Room.databaseBuilder(
                context,
                FavDatabase::class.java,
                DB_NAME
            ).build()
    }
}