package cz.mokripat.emapp.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import cz.mokripat.emapp.R
import cz.mokripat.emapp.data.network.model.Participant
import cz.mokripat.emapp.data.network.model.ParticipantType
import de.hdodenhof.circleimageview.CircleImageView

interface OnRowListener {
    fun onRowClicked(id: String)
}

class ParticipantsViewHolder(row: View) : RecyclerView.ViewHolder(row) {
    val nameView: TextView = itemView.findViewById(R.id.participant_name)
    val imageView: CircleImageView = itemView.findViewById(R.id.profile_image)
    val statusView: TextView = itemView.findViewById(R.id.participant_status)
    val osPrefView: ImageView = itemView.findViewById(R.id.ospref_icon)
}

class ParticipantsAdapter(
    participants: List<Participant>,
    private val onRowListener: OnRowListener
) : RecyclerView.Adapter<ParticipantsViewHolder>() {

    var participants: List<Participant> = participants
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParticipantsViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.participant_row, parent, false)
        val holder = ParticipantsViewHolder(view)
        holder.itemView.setOnClickListener {
            val item = participants[holder.adapterPosition]
            val id = item.id
            onRowListener.onRowClicked(id)
        }
        return holder
    }

    override fun onBindViewHolder(holder: ParticipantsViewHolder, position: Int) {
        val item = participants[position]
        holder.nameView.text = item.name
        Glide.with(holder.imageView)
            .load(item.icon192)
            .error(R.drawable.error_icon)
            .into(holder.imageView)
        holder.statusView.text = item.title
        holder.osPrefView.setImageResource(
            when (item.participantType) {
                ParticipantType.ANDROID_MENTOR, ParticipantType.ANDROID_STUDENT -> R.drawable.ic_android_studio_icon
                ParticipantType.IOS_MENTOR, ParticipantType.IOS_STUDENT -> R.drawable.apple_icon_rainbow
                else -> R.drawable.watcher_icon
            }
        )
    }

    override fun getItemCount(): Int {
        return participants.size
    }

}