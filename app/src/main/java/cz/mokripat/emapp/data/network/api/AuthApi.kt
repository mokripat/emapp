package cz.mokripat.emapp.data.network.api

import cz.mokripat.emapp.data.network.model.dto.AccessToken
import cz.mokripat.emapp.data.network.model.dto.LoginRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {
    @POST("v2/login")
    suspend fun login(@Body loginRequest: LoginRequest): AccessToken
}