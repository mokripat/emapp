package cz.mokripat.emapp.utils

import android.content.SharedPreferences
import androidx.core.content.edit
import cz.mokripat.emapp.view.LoginActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CredentialsLoader(private val sharedPrefs: SharedPreferences) {
    var userId: String = ""
        private set
    var password: String = ""
        private set
    var shouldRemember: Boolean = false
        private set

    fun load(scope: CoroutineScope) {
        scope.launch(Dispatchers.IO) {
            if (sharedPrefs.getBoolean(LoginActivity.SP_REMEMBER, false)) {
                userId = sharedPrefs.getString(LoginActivity.SP_USER_ID, "").orEmpty()
                password = sharedPrefs.getString(LoginActivity.SP_PASSWORD, "").orEmpty()
                shouldRemember = true
            }
        }
    }

    fun save(scope: CoroutineScope, userId: String, password: String) {
        scope.launch(Dispatchers.IO) {
            sharedPrefs.edit {
                putString(LoginActivity.SP_USER_ID, userId)
                putString(LoginActivity.SP_PASSWORD, password)
                putBoolean(LoginActivity.SP_REMEMBER, true)
            }
        }
    }

    fun clear(scope: CoroutineScope) {
        scope.launch(Dispatchers.IO) {
            sharedPrefs.edit {
                putString(LoginActivity.SP_USER_ID, "")
                putString(LoginActivity.SP_PASSWORD, "")
                putBoolean(LoginActivity.SP_REMEMBER, false)
            }
        }
    }
}