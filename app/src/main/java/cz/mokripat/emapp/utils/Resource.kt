package cz.mokripat.emapp.utils

sealed class Resource {

    object Success : Resource()

    object Loading : Resource()

    object NetworkError : Resource()

    object IOError : Resource()

    object LoginNeeded : Resource()

    object Unauthorized: Resource()
}