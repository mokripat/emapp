package cz.mokripat.emapp.utils

enum class ParticipantListState {
    ALL, ANDROID, IOS
}