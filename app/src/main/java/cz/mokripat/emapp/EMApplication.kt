package cz.mokripat.emapp

import android.app.Application
import android.content.Context
import cz.mokripat.emapp.data.database.FavDatabase
import cz.mokripat.emapp.data.network.RestClient
import cz.mokripat.emapp.repository.ParticipantListRepository
import cz.mokripat.emapp.repository.ParticipantRepository
import cz.mokripat.emapp.utils.CredentialsLoader
import cz.mokripat.emapp.view.LoginActivity
import cz.mokripat.emapp.viewmodel.LoginViewModel
import cz.mokripat.emapp.viewmodel.ParticipantListViewModel
import cz.mokripat.emapp.viewmodel.ParticipantViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class EMApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@EMApplication)
            modules(mainModule)
        }
    }

    private val mainModule = module {
        single { RestClient(androidApplication()) }
        single { get<RestClient>().participantsApi }
        single { get<RestClient>().authApi }

        single { FavDatabase.create(androidApplication()) }
        single {
            androidApplication().getSharedPreferences(
                LoginActivity.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE
            )
        }
        single { get<FavDatabase>().getFavParticipantDao() }

        factory { ParticipantRepository(get(), get()) }
        factory { ParticipantListRepository(get()) }

        viewModel { ParticipantListViewModel(androidApplication(), get()) }
        viewModel { ParticipantViewModel(androidApplication(), get()) }
        viewModel {
            LoginViewModel(
                get(),
                get(),
                CredentialsLoader(get())
            )
        }
    }
}